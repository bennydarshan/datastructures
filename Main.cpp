#include<iostream>
#include "Maze.h"

int main()
{
	int selection = -1;
	Maze m1;

	std::cout << "Maze: 1) From input 2) Random\n";
	std::cin >> selection;
	switch(selection) {
	case 1: if(m1.get_maze()) return 1;
		break;
	case 2: m1.generate();
		break;
	}
	if(m1.solve_maze()) return 1; 
	//std::cout << "Solved Maze:\n";
	m1.print();
	
	return 0;
}
