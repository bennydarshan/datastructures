template<class T>
class Heap{
private:
	T *data = nullptr;
	int lsize = 0;
	int psize = 0;
	int parent(int n)
	{
		return n/2;
	}
	int left(int n)
	{
		return 2*n;
	}
	int right(int n)
	{
		return 2*n + 1;
	}
	void fixHeap(int n)
	{
		while(n < lsize) {
			int _max = n;
			int _left = left(n);
			int _right = right(n);
			int _parent = parent(n);
			if(_left < lsize) _max = (data[n] < data[_left]) ? n : _left;
			if(_right < lsize) _max = (data[_max] < data[_right]) ? _max : _right;
			if(_max == n) return;
			std::swap(data[n], data[_max]);
			n = _max;

			
		}

	}
public:
	Heap(int size): psize(size), lsize(0)
	{
		data = new T[size];
	}
	Heap(T arr[], int size): psize(size), lsize(size)
	{
		data = new T[size];
		std::copy(arr, arr + size, data);
		for(int i = parent(size); i>=0;i--)
			fixHeap(i);
	}
	T &min()
	{
		return *data;
	}
	T deleteMin()
	{
		auto ret = data[0];
		data[0] = data[lsize-1];
		--lsize;
		fixHeap(0);
		return ret;
	}
	void insert(T new_item)
	{
		assert(lsize < psize);
		int i = lsize;
		data[lsize++] = new_item;

		while((i>0) && (data[parent(i)] > data[i])) {
			std::swap(data[parent(i)], data[i]);
			i = parent(i);
		}
	}
	void print()
	{
		for(auto iter = data; iter < data + lsize; ++iter)
			std::cout << *iter << ' ';
		std::cout << '\n';
	}
};
