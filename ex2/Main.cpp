#include<cstdio>
#include<iostream>
#include<cassert>
#include<cstdlib>
#include<ctime>
#include<cstring>

#include "Tree.h"
#include "Heap.h"
#include "Select.h"

struct Person{
	static int *comp;
	int key;
	char name[64];
};

int *Person::comp = nullptr;

bool operator==(const Person &self, const Person &other)
{
	if(Person::comp) ++(*Person::comp);
	return self.key == other.key;
}
bool operator>(const Person &self, const Person &other)
{
	if(Person::comp) ++(*Person::comp);
	return self.key > other.key;
}
bool operator<(const Person &self, const Person &other)
{
	if(Person::comp) ++(*Person::comp);
	return self.key < other.key;
}

const Person &RandSelect(Person arr[], int size, int k, int &comp)
{
	Person::comp = &comp;
	auto &ret = kth(arr, size, k);
	Person::comp = nullptr;
	return ret;

}

const Person &selectHeap(Person arr[], int size, int k, int &comp)
{
	Person::comp = &comp;
	Heap<Person> h1(arr, size);
	for(int i=0;i<k;i++) h1.deleteMin();
	auto &ret = h1.min();

	Person::comp = nullptr;
	return ret;
}

int main()
{
	Person *arr = nullptr;
	int size = 0, k = -1;
	int comp_select = 0, comp_heap = 0, comp_tree = 0;
	srand(time(NULL));
	scanf("%d", &size);
	//printf("%d\n", size);
	arr = new Person[size];
	for(auto *iter = arr ;iter<arr+size;++iter) scanf("%d %s", &(iter->key), iter->name);
	scanf("%d", &k);
	--k; //k is zero based in my algorithms
	auto res = RandSelect(arr, size, k, comp_select);
	printf("%d %s\nRandSelection: %d comparisons\n", res.key, res.name, comp_select);
	auto res = selectHeap(arr, size, k, comp_heap);
	printf("%d %s\nheapSelection: %d comparisons\n", res.key, res.name, comp_heap);

	//for(auto *iter = arr; iter<arr+size;++iter) printf("%d %s\n", iter->key, iter->name);

	return 0;
}
