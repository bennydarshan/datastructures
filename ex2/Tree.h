template<class T>
class Tree{
private:
	struct _node {
		T data;
		struct _node *left;
		struct _node *right;
	};
	struct _node *root;

	struct _node *insert(struct _node *node, T v)
	{
		if(!node) {
			auto ret = new struct _node;
			ret->data = v;
			ret->left = nullptr;
			ret->right = nullptr;
			return ret;
		}
		if(node->data < v) {
			node->right = insert(node->right, v);
		}
		else {
			node->left = insert(node->left, v);
		}
		return node;
	}
	T find(struct _node *node, T v)
	{
		assert(node);
		if(node.data == v) return node.data;
		if(node.data < v) return find(node->right, v);
		return find(node->left, v);
	}
	void inOrder(struct _node *node)
	{
		if(!node) return;
		inOrder(node->left);
		std::cout << node->data << ' ';
		inOrder(node->right);
	}

public:
	Tree():root(nullptr){}
	void insert(T v)
	{
		root = insert(root, v);
	}
	T find(T v)
	{
		return find(root, v);
	}
	void inOrder()
	{
		inOrder(root);
	}


};
