#pragma once

template<class T>
int partition(T arr[], int size)
{
	int pivot_i = rand()%size;
	int offset = 0;
	const T pivot = arr[pivot_i];
	for(int i =0;i<size;i++)
	{
		if(arr[i] < pivot) {
			if(offset == pivot_i) pivot_i = i; //if we are moving the pivot itself, we need to update
			std::swap(arr[i], arr[offset]);
			offset++;
		}
	}
	std::swap(arr[pivot_i], arr[offset]);
	return offset;
}

template<class T>
T &kth(T arr[], int size, int k)
{
	int test = partition(arr, size);
	if(test == k) return arr[test];
	if(test > k) return kth(arr, test + 1, k);
	return kth(arr + test, size - test, k - test);
}
