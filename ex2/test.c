#include<stdio.h>
#include<stdlib.h>
#include<time.h>

void swap(int *a, int *y)
{
	int tmp = *a;
	*a = *y;
	*y = tmp;
}

int parition(int arr[], int size)
{
	int pivot_i = rand() % size;
	int offset = 0;
	int pivot = arr[pivot_i];
	printf("The pivot is %d\n", pivot);
	for(int i=0;i<size;i++) {
		if(arr[i] < pivot) {
			if(offset == pivot_i) pivot_i = i;
			swap(arr + i, arr + offset);
			offset++;
		}
	}
	swap(arr + pivot_i, arr + offset);
	return offset;
}

int kth(int arr[], int size, int k)
{
	int test = parition(arr, size);
	if(test == k) return arr[test];
	if(test > k) return kth(arr, test + 1, k);
	return kth(arr + test, size - test, k - test); 

}

void print(int arr[], int size)
{
	for(int i=0;i<size;i++) printf("%d ", *(arr + i));
	printf("\n");
}

int main()
{
	srand(time(NULL));
	int arr[] = {5, 2, 8, 23, 1, 10};
	int size = 6;
	//int result = parition(arr, size);
	int result = kth(arr, size, 3);
	printf("The result is %d\n", result);
	print(arr, size);
	return 0;
}
