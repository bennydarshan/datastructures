#pragma once

template <class T>
class MyQue {
	T *data = nullptr;
	unsigned int size = 0;
	T *head = nullptr;
	T *empty = nullptr;
public:
	MyQue(int init_size) : size( init_size)
	{
		data = new T[init_size];
		empty = data;
	}
	~MyQue()
	{
		delete[] data;
	}
	void push(T new_item)
	{
		*empty = new_item;
		if(!head) head = empty;
		empty++;
		if(empty >= data + size) { 
			empty = data;
		}
	}
	T pop()
	{
		assert(head);
		T temp = *head;
		head++;
		if(head >= data + size) head = data;
		if(head == empty) head = nullptr;
		return temp;

	}
	bool isEmpty()
	{
		return head == nullptr;
	}
};
