#include <iostream>
#include <cassert>
#include <string>
#include <cstdlib>

#include "MyStack.h"
#include "MyQue.h"
#include "Maze.h"

typedef struct{
	int x;
	int y;
}coord;

bool operator==(const coord &self, const coord &other)
{
	return (self.x == other.x) && (self.y == self.y);
}

coord operator+(const coord &self, const coord &other)
{
	return {self.x + other.x, self.y + other.y};
}

coord operator/(const coord &self, const int &n)
{
	return {self.x/n, self.y/n};
}

Maze::Maze() 
{
	for(int i=0;i<25;i++)for(int j=0;j<80;j++) maze[i][j] = '\0';
}

int Maze::get_maze()
{
	std::cin >> h >> w ;



	char ch;

	std::cin.get();

	for(int i=0;i<h;i++) {
		std::cin.getline(maze[i],80);
		if(maze[i][w] != '\0') {
			std::cerr << "invalid input\n";
			return 1;
		}
	}

	for(int i=0;i<h;i++) {
		if((maze[i][0] != '*') && (i != 1)) {
			std::cerr << "invalid input\n";
			return 1;
		}
		if((maze[i][w-1] != '*') && (i != h-2)) {
			std::cerr << "invalid input\n";
			return 1;
		}
	}

	for(int i=0;i<w;i++) {
		if(maze[0][i] != '*') {
			std::cerr << "invalid input\n";
			return 1;
		}
		if(maze[h-1][i] != '*') {
			std::cerr << "invalid input\n";
			return 1;
		}
	}

	for(int i=0;i<h;i++) for(int j=0;j<w;j++) {
		if(maze[i][j] == '*') continue;
		else if((maze[i][j] == ' ') || (maze[i][j] == '\0') || (maze[i][j] == '\n')) maze[i][j] = ' ';
		else {
			std::cerr << "invalid input\n";
			return 1;
		}
	}


	if(maze[1][0] != ' ') {
		std::cerr << "invalid input\n";
		return 1;
	}
	if(maze[h-2][w-1] != ' ') {
		std::cerr << "invalid input\n"; 
		return 1;
	}

	return 0;

}

int Maze::solve_maze()
{
	int empty_spaces = 0;
	for(int i=0;i<h;i++)for(int j=0;j<w;j++)
		if(maze[i][j] == ' ') empty_spaces++;
	MyQue<coord> q1(empty_spaces);
	q1.push({0, 1});
	const coord exit = {w-1, h-2};
	while(!q1.isEmpty()) {
		auto iter = q1.pop();
		maze[iter.y][iter.x] = '$';
		if(iter == exit) return 0;
		if(maze[iter.y + 1][iter.x] == ' ') q1.push({iter.x, iter.y + 1});
		if(maze[iter.y - 1][iter.x] == ' ') q1.push({iter.x, iter.y - 1});
		if(maze[iter.y][iter.x + 1] == ' ') q1.push({iter.x + 1, iter.y});
		if(maze[iter.y][iter.x - 1] == ' ') q1.push({iter.x - 1, iter.y});

	}
	std::cerr << "no solution\n";
	return 1;
}

int Maze::generate_init()
{
	for(int i=1;i<h-1;i++)for(int j=1;j<w-1;j++) {
		if((i%2 == 1) && (j%2 == 1)) maze[i][j] = ' ';
		else maze[i][j] = '*';
	}
	for(int i=0;i<h;i++) {
		maze[i][0] = '*';
		maze[i][w-1] = '*';
	}
	for(int i=0;i<w;i++) {
		maze[0][i] = '*';
		maze[h-1][i] = '*';
	}
	maze[1][0] = ' ';
	maze[h-2][w-1] = ' ';

	return 0;
}

int Maze::open_up()
{
	MyStack<coord> s1;
	s1.push({1, 1});
	const coord offsets[] = { {0, 2}, {2, 0}, {-2, 0}, {0, -2} };
	while(!s1.isEmpty()) {
		auto iter = s1.pop();
		int avail_index[4];
		int avail_size = 0;
		maze[iter.y][iter.x] = '$';
		for(int i = 0; i < 4; i++) { //check available neighbors
			const coord cur = iter + offsets[i];
			if((cur.x < 0) || (cur.x >= w) || (cur.y < 0) || (cur.y >= h)) continue;
			if(maze[cur.y][cur.x] == ' ') avail_index[avail_size++] = i;

		}
		if(!avail_size) continue;
		const int i = avail_index[rand()%avail_size];
		const coord neighb = offsets[i] + iter;
		//maze[neighb.y][neighb.x] = '$';
		const coord remove = iter + offsets[i]/2;
		maze[remove.y][remove.x] = ' ';
		s1.push(iter);
		s1.push(neighb);



	}

	for(int i=0;i<h;i++)for(int j=0;j<w;j++) if(maze[i][j] == '$') maze[i][j] = ' '; //cleaning up the markers
		
	return 0;
}

int Maze::generate()
{
	std::cout << "Insert the dimensions of the maze w, h\n";
	std::cin >> w >> h;
	std::cin.get();
	generate_init();
	open_up();
	return 0;
}
void Maze::print()
{
	for(int i=0;i<h;i++)
		std::cout << maze[i] << '\n';

}

