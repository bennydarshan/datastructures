#pragma once

template <class T>
class MyStack {
	struct _node{
		T data;
		struct _node *next;
	};
	struct _node *head;
public:
	MyStack(): head(nullptr) {}
	~MyStack() {
		while(head) {
			struct _node *temp = head;
			head = head->next;
			delete[] temp;
		}
	}
	void push(T new_item)
	{
		struct _node *new_node = new struct _node;
		new_node->data = new_item;
		new_node->next = head;
		head = new_node;
	}
	T pop()
	{
		assert(head);
		T tmp = head->data;
		auto tmp_node = head;
		head = head->next;
		delete[] tmp_node;
		return tmp;
	}
	bool isEmpty()
	{
		return (head == nullptr);
	}


};
