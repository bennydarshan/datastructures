#pragma once




class Maze{
private:
	char maze[25][80];
	int h,w;
public:
	Maze();
	int get_maze();
	int solve_maze();
	int generate_init();
	int open_up();
	int generate();
	void print();
};

